""" An example of how to use the entity system. """

#  Matthew Westbrook
#
#  An example that demonstrates how to use the entity system.
#
#  * The first task is to define your entity systems and components.
#  * Then initialize the world and add the entity systems to the world.
#  * Construct entities and add their components.
#  * Update the world when a tick occurs.

from entsystem.entity_system import EntitySystem
from entsystem.world import World

class PrintableComponent(object):
    """
    Component that countains a message to print.

    Attributes:
        printable: A string containing the message to be printed for the
            entity.
    """
    def __init__(self, text):
        self.printable = text

    def set_printable(self, text):
        """Set printed text."""
        self.printable = text

    def get_printable(self):
        """Return text to be printed."""
        return self.printable

class PrintSystem(EntitySystem):
    """
    A EntitySystem that prints printable entities.
    """
    def __init__(self):
        super().__init__([PrintableComponent])

    def update(self, entity, delta_time):
        """Overriden method from EntitySystem that is called when the world
        updates and procedes to print the entity printable attribute.

        This is where most of the EntitySystem's work happens. This method
        shouldn't be called by the user. Instead EntitySystem calls this
        method each tick to update entities.

        Args:
             entity: The entity currenty being updated.
             delta_time: Time change since last tick.
        """
        for _ in range(delta_time):
            print(entity.get_component(PrintableComponent).get_printable())

def main():
    """
    Creates a world.
    Populates world with entities and an entity system that prints attributes
    of those entities.
    """
    # Setup the world and entity systems
    world = World()
    printsystem = PrintSystem()
    world.add_system(printsystem)

    # Create entities
    ent = world.create_entity()
    ent2 = world.create_entity()

    # Add components to the entities
    world.add_component(ent, PrintableComponent("Entity1"))
    world.add_component(ent2, PrintableComponent("Entity2"))


    # Update world when a tick occurs.
    print("-------\nLiving Entities:")
    world.update(1)

    print("-------\nDelete entity2")
    world.remove_component(ent2, PrintableComponent)

    print("-------\nLiving Entities:")
    world.update(1)

    print("-------\nDelete PrintSystem")
    world.remove_system(PrintSystem)

    print("------\nLiving Entities:")
    world.update(1)

if __name__ == "__main__":
    main()
