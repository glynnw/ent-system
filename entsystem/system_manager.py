"""system_manager

Classes:
    SystemManager: Manages all systems in the game world.
"""
#  Matthew Westbrook
#
#  The system manager handles adding, storing, and removing systems from the
#  game world as well as the proper inclusion of entities to systems
#  entities belong to.

class SystemManager(object):
    """SystemManager

    SystemManager manages and stores all the EntitySystems in simulation/game's
    world.
    """
    def __init__(self):
        self.__systems = dict()

    def filter_entity(self, entity):
        """Adds the entity to systems it belongs to by filtering the systems
        based on their needed components and the components the entity has."""
        for system in self.__systems.values():
            if all(component in entity.component_names() for component in
                    system.required_components()):
                system.add_entity(entity)
            else:
                system.remove_entity(entity)

    def remove_entity(self, entity):
        """Remove entity from all systems."""
        for system in self.__systems.values():
            system.remove_entity(entity)

    def add_system(self, system):
        """Add entity to system"""
        self.__systems[system] = system

    def remove_system(self, system_type):
        """Remove system."""
        return self.__systems.pop(system_type, False)

    def get_system(self, system):
        """Get system."""
        return self.__systems.get(system, None)

    def get_systems(self):
        return list(self.__systems.keys())

    def update(self, delta_time):
        """Notifies all systems tracked that a tick occured."""
        for system in self.__systems.values():
            system.process(delta_time)
