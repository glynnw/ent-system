"""
EntityManager class

Manages the life of an entity and access to the entity. Spawns are nothing more
than entities that are living or spawned into the world.
"""
from entsystem.entity import Entity

class EntityManager(object):
    """EntityManager"""

    def __init__(self):
        self.__next_eid = 0
        self.__entities = dict()
        self.__dead_entities = []
        # self.__sys_manager = sys_manager

    def spawn_entity(self):
        """Spawn a managed entity."""
        eid = self.__aquire_eid()
        entity = Entity()
        entity.spawn(eid)
        self.__entities[eid] = entity
        # self.__add_to_systems(entity)
        return eid

    def __aquire_eid(self):
        """
        Private method that aquires an id by either recycling a dead spawn id
        or creating a new one.
        """
        if self.__dead_entities:
            eid = self.__dead_entities.pop()
        else:
            eid = self.__next_eid
            self.__next_eid += 1
        return eid

    def delete_spawn(self, eid):
        """Delete managed spawn."""
        if self.__entities.pop(eid, False):
            self.__dead_entities.append(eid)
            # self.__notify_systems_of_remove(eid)
            return True
        else:
            return False

    def get_spawn(self, eid):
        """Return a spawn's eid."""
        return self.__entities.get(eid, False)

    def spawns(self):
        """Returns a list of managed spawns."""
        return list(self.__entities.values())

    # def __notify_systems_of_remove(self, eid):
    #     """
    #     Private method that notifies the system manager that the
    #     entity(spawn) is no longer existing.
    #     """
    #     self.__sys_manager.remove_entity(self.get_spawn(eid))

    # def __add_to_systems(self, entity):
    #     """
    #     Private method that lets the system manager know an entity needs to be
    #     checked if it belongs to any entity systems.
    #     """
    #     self.__sys_manager.filter_entity(entity)
