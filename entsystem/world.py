"""world

World is the glue that binds System and Entity managers.
"""
from entsystem.system_manager import SystemManager
from entsystem.entity_manager import EntityManager

class World(object):
    """World
    World is the glue that combines the SystemManager and EntityManager together.
    """
    def __init__(self):
        self.__system_manager = SystemManager()
        self.__entity_manager = EntityManager()

    def update(self, delta_time):
        """Sends a message to the SystemManager to update the systems that a
        tick has occured."""
        self.__system_manager.update(delta_time)
