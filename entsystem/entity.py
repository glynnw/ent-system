"""entity
    classes:
        Entity: A container for components with a unique eid.
"""
class Entity(object):
    """Entity
    Entity is really more of a useful container than a needed one. Entity
    manages an entity's components and associates it with an unique eid.

       Methods:
          add_component
          remove_component
          get_component
          component_names
          get_id
          spawn
    """

    def __init__(self):
        self._components = dict()
        self._eid = -1

    def add_component(self, component):
        """Add component to entity."""
        self._components[type(component)] = component

    def get_component(self, component_type):
        """Get component of type component_type
            Returns: component_type
        """
        return self._components.get(component_type, False)

    def spawn(self, eid):
        """Assigns the eid of the entity."""
        self._eid = eid

    def remove_component(self, component_type):
        """Remove component from entity."""
        return self._components.pop(component_type, False)

    def component_names(self):
        """Returns a list of the names of the entity's components."""
        return list(self._components.keys())

    def get_id(self):
        """Returns the eid of the entity."""
        return self._eid
