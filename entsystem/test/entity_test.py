from entsystem.entity import Entity

class TestEntity:

    def test_add_component(self):
        component = "Hello"
        entity = Entity()
        entity.add_component(component)
        assert entity.get_component(str) is "Hello"

    def test_remove_component(self):
        component = "Hello"
        entity = Entity()
        entity.add_component(component)
        entity.remove_component(str)
        assert not entity.component_names()
