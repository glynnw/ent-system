from entsystem.entity_manager import EntityManager

class TestEntityManager:

    def test_create_entity_returns_eid(self):
        entity_manager = EntityManager()
        entity = entity_manager.spawn_entity()
        assert type(entity) is int

    def test_create_entity_returns_unique_eid(self):
        entity_manager = EntityManager()
        entity1 = entity_manager.spawn_entity()
        entity2 = entity_manager.spawn_entity()
        assert entity1 is not entity2

