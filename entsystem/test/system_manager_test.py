from entsystem.system_manager import SystemManager
from entsystem.entity_system import EntitySystem

class TestSystemManager:

    def test_get_and_add_system(self):
        sys_manager = SystemManager()
        system = TestSystem()
        sys_manager.add_system(system)
        assert sys_manager.get_system(system) is system

    def test_remove_system(self):
        sys_manager = SystemManager()
        system = TestSystem()
        sys_manager.add_system(system)
        assert sys_manager.get_system(system) is system
        sys_manager.remove_system(system)
        assert sys_manager.get_system(system) is None

class TestSystem(EntitySystem):
    def __init__(self):
        super().__init__([])

    def update(self, entity, delta_time):
        pass
