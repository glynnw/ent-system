"""entity_system
EntitySystem processes entities and updates them.
Entity systems should inherit and overide the update method.
The update method is passed an entity and delta time by SystemManager.
Additionally the EntitySystem should specify its required components so that
entities can be filtered and assigned to the EntitySystem.

The update method should contain the logic for the entities that belong to the
system.
"""
class EntitySystem(object):
    """EntitySystem
       EntitySystem is the superclass that is inherited to create a
       EntitySystem. The subclass should override the update method
       which will be called for each entity in the entity system. The logic for
       entities belonging to the system goes in the overridden update method.
    """
    def __init__(self, required_components):
        self._required_components = required_components
        self._entities = dict()

    def process(self, delta_time):
        """Process the entities."""
        for entity in self._entities.values():
            self.update(entity, delta_time)

    def update(self, entity, delta_time):
        """Update entities. Override this in the inherited class."""
        pass

    def required_components(self):
        """Return a list of the system's required components."""
        return self._required_components

    def set_required_components(self, array):
        """Set the list of required components."""
        self._required_components = array

    def add_entity(self, entity):
        """Add entity to list of entities belonging to the system."""
        self._entities[entity.get_id()] = entity

    def remove_entity(self, entity):
        """Remove entity from the entities belonging to the system."""
        return self._entities.pop(entity.get_id(), False)
